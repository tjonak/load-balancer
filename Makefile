.PHONY: all
all:
	go build ./cmd/lb

.PHONY: test
test:
	go test -race ./...