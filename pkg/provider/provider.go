package provider

import (
	"fmt"
	"time"
)

// Provider represents any upstream for this load-balancer
type Provider interface {
	Get() string
	Check() error
}

// Type denotes provider type
type Type int

const (
	// TypeHealthy provider always works
	TypeHealthy Type = iota
	// TypeFaulty provider always fails
	TypeFaulty
	// TypeFaultyThatRecovers provider fails initially then passes healthcheck
	TypeFaultyThatRecovers
	// TypeSlow provider responds after a delay
	TypeSlow
)

// NewProvider creates provider instance
func NewProvider(id string, t Type) Provider {
	switch t {
	case TypeFaulty:
		return &faultyProvider{id: id}
	case TypeFaultyThatRecovers:
		return &faultyThatRecoversProvider{id: id}
	case TypeSlow:
		return &slowProvider{id: id, delay: time.Second}
	default:
		return &provider{id: id}
	}
}

type provider struct {
	id string
}

func (p *provider) Get() string {
	return p.id
}

func (p *provider) String() string {
	return p.id
}

func (p *provider) Check() error {
	return nil
}

type faultyProvider struct {
	id string
}

func (fp *faultyProvider) Get() string {
	return fp.id
}

func (fp *faultyProvider) String() string {
	return fp.id
}

func (fp *faultyProvider) Check() error {
	return fmt.Errorf("provider %s is faulty", fp.id)
}

type faultyThatRecoversProvider struct {
	id        string
	callCount int
}

func (ftrp *faultyThatRecoversProvider) Get() string {
	return ftrp.id
}

func (ftrp *faultyThatRecoversProvider) String() string {
	return ftrp.id
}

func (ftrp *faultyThatRecoversProvider) Check() error {
	ftrp.callCount++
	if ftrp.callCount == 1 {
		return fmt.Errorf("provider %s is faulty", ftrp.id)
	}
	return nil
}

type slowProvider struct {
	id    string
	delay time.Duration
}

func (sp *slowProvider) Get() string {
	<-time.After(sp.delay)
	return sp.id
}

func (sp *slowProvider) String() string {
	return sp.id
}

func (sp *slowProvider) Check() error {
	return nil
}
