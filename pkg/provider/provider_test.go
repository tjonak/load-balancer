package provider

import "testing"

func TestProviderGet(t *testing.T) {
	const providerName = "p1"

	p1 := NewProvider(providerName, TypeHealthy)
	if name := p1.Get(); name != providerName {
		t.Errorf("Mismatch (expected: %s, got: %s)", providerName, name)
	}
}
