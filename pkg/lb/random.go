package lb

import (
	"math/rand"

	"gitlab.com/tjonak/load-balancer/pkg/provider"
)

type randomUpstreamSelector struct {
	lastProviderID uint
}

func newRandomUpstreamSelector() *randomUpstreamSelector {
	return &randomUpstreamSelector{}
}

func (rus *randomUpstreamSelector) Choose(providers []provider.Provider) provider.Provider {
	upstream := providers[rand.Intn(len(providers))]

	return upstream
}
