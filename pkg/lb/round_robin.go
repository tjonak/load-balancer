package lb

import (
	"gitlab.com/tjonak/load-balancer/pkg/provider"
)

type roundRobinUpstreamSelector struct {
	nextProviderID uint
}

func newRoundRobinUpstreamSelector() *roundRobinUpstreamSelector {
	return &roundRobinUpstreamSelector{}
}

func (rrus *roundRobinUpstreamSelector) Choose(providers []provider.Provider) provider.Provider {
	// in case providers list got truncated
	currentID := rrus.nextProviderID % uint(len(providers))

	upstream := providers[currentID]
	rrus.nextProviderID = (currentID + 1) % uint(len(providers))

	return upstream
}
