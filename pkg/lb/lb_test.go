package lb

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestRegister(t *testing.T) {
	tcs := []struct {
		desc        string
		limit       uint
		count       uint
		expectedErr bool
	}{
		{limit: 1, count: 1},
		{limit: 1, count: 2, expectedErr: true},
		{limit: 3, count: 2},
	}
	for _, tc := range tcs {
		t.Run(tc.desc, func(t *testing.T) {
			lb := NewLoadBalancer(TypeRandom, tc.limit, time.Duration(0), 10)

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			go lb.Run(ctx)

			var err error
			for i := uint(0); i < tc.count; i++ {
				err = lb.Register(fmt.Sprintf("p%d", i))
				if err != nil {
					break
				}
			}

			if (err != nil) != tc.expectedErr {
				t.Errorf("Mismatch (expectedErr: %t, err: %v)", tc.expectedErr, err)
			}
		})
	}
}
