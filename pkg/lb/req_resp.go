package lb

import "gitlab.com/tjonak/load-balancer/pkg/provider"

type getRequest struct {
	respChan   chan<- *getResponse
	retryCount uint32

	// uuid/xid in real world case
	requestID int
}

type getResponse struct {
	ID  string
	err error
}

type registerRequest struct {
	id           string
	providerType provider.Type
	respChan     chan<- *registerResponse
}

type registerResponse struct {
	err error
}

type excludeRequest struct {
	id       string
	respChan chan<- struct{}
}
