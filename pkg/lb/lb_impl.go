package lb

import (
	"context"
	"fmt"
	"log"
	"sync/atomic"
	"time"

	"gitlab.com/tjonak/load-balancer/pkg/provider"
)

const (
	lbBacklogSize           = 10000
	registerBacklogSize     = 100
	excludeBacklogSize      = 100
	healthyRecheckThreshold = 2
)

type baseLoadbalancer struct {
	requestsChan chan *getRequest
	registerChan chan *registerRequest
	excludeChan  chan *excludeRequest

	upstreamRequestTimeout time.Duration
	responseSendTimeout    time.Duration
	getRequestSendTimeout  time.Duration
	lbResponseAwaitTimeout time.Duration

	healthCheckInterval     time.Duration
	healthyRecheckThreshold int

	// maxRetries uint32
	limit           uint
	providers       []provider.Provider
	faultyProviders map[provider.Provider]int

	maxConcurrentRequestsPerProvider uint32
	requestsInFlight                 int64

	upstreamSelector upstreamSelector
}

type upstreamSelector interface {
	Choose([]provider.Provider) provider.Provider
}

func newLoadBalancer(
	limit uint,
	us upstreamSelector,
	healthCheckInterval time.Duration,
	maxConcurrentRequestsPerProvider uint32,
) *baseLoadbalancer {
	if us == nil {
		panic("nil upstream selector")
	}

	return &baseLoadbalancer{
		limit:        limit,
		requestsChan: make(chan *getRequest, lbBacklogSize),
		registerChan: make(chan *registerRequest, registerBacklogSize),
		excludeChan:  make(chan *excludeRequest, excludeBacklogSize),

		upstreamSelector: us,
		faultyProviders:  map[provider.Provider]int{},

		healthCheckInterval:     healthCheckInterval,
		healthyRecheckThreshold: healthyRecheckThreshold,

		maxConcurrentRequestsPerProvider: maxConcurrentRequestsPerProvider,

		upstreamRequestTimeout: time.Second * 30,
		responseSendTimeout:    time.Second * 30,
		getRequestSendTimeout:  time.Second * 30,
		lbResponseAwaitTimeout: time.Second * 30,
	}
}

func (blb *baseLoadbalancer) Run(ctx context.Context) {
	log.Printf("Load balancer started")

	var tickerC <-chan time.Time
	if blb.healthCheckInterval != 0 {
		log.Printf("Registering health check routine")
		healthCheckTicker := time.NewTicker(blb.healthCheckInterval)
		tickerC = healthCheckTicker.C
		defer healthCheckTicker.Stop()
	}

	for {
		select {
		case <-ctx.Done():
			log.Printf("ctx.Done, wrapping up")
			return
		case req := <-blb.requestsChan:
			blb.handleGet(req)
		case req := <-blb.registerChan:
			blb.handleRegister(req)
		case req := <-blb.excludeChan:
			blb.handleExclude(req)
		case <-tickerC:
			blb.handleHealthCheck()
		}
	}
}

func (blb *baseLoadbalancer) Get() (string, error) {
	respC := make(chan *getResponse, 1)
	select {
	case blb.requestsChan <- &getRequest{respChan: respC}:
	case <-time.After(blb.getRequestSendTimeout):
		return "", fmt.Errorf("Timeout while sending get request to load balancer")
	}

	select {
	case resp := <-respC:
		return resp.ID, resp.err
	case <-time.After(blb.lbResponseAwaitTimeout): // this timeout should be controlled by client
		return "", fmt.Errorf("timeout while waiting for load balancer response")
	}
}

func (blb *baseLoadbalancer) handleGet(req *getRequest) {
	resultC := make(chan string, 1)
	currRequests := atomic.LoadInt64(&blb.requestsInFlight)
	// In real world rate limit exceeded should be a separate event from no providers
	if currRequests >= int64(len(blb.providers)*int(blb.maxConcurrentRequestsPerProvider)) {
		select {
		case req.respChan <- &getResponse{err: ErrRateLimitExceeded}:
		default:
			log.Printf("sending error response to get request would block, dropping (reqID: %d)", req.requestID)
		}
		return
	}
	atomic.AddInt64(&blb.requestsInFlight, 1)

	upstream := blb.upstreamSelector.Choose(blb.providers)

	// upstream.Get() blocks we leak goroutine, in real world case this would be a network call with a deadline attached
	// for sake of simplicity not messing with upstream
	go func() { resultC <- upstream.Get() }()

	go func() {
		defer atomic.AddInt64(&blb.requestsInFlight, -1)
		var result string
		select {
		case result = <-resultC:
		case <-time.After(blb.upstreamRequestTimeout):
			// could retry different one here, keep retries state in request
			log.Printf("Upstream timed out (duration: %s, requestID: %d)", blb.upstreamRequestTimeout, req.requestID)
			return
		}

		select {
		case req.respChan <- &getResponse{ID: result}:
		case <-time.After(blb.responseSendTimeout):
			log.Printf("Couldn't handle request (duration: %s, requestID: %d)", blb.responseSendTimeout, req.requestID)
		}
	}()
}

func (blb *baseLoadbalancer) Register(providerID string) error {
	return blb.RegisterTyped(providerID, provider.TypeHealthy)
}

func (blb *baseLoadbalancer) RegisterTyped(providerID string, providerType provider.Type) error {
	respC := make(chan *registerResponse, 1)
	select {
	case blb.registerChan <- &registerRequest{respChan: respC, id: providerID, providerType: providerType}:
	case <-time.After(blb.getRequestSendTimeout):
		return fmt.Errorf("Timeout while sending register request to load balancer: %s", providerID)
	}

	select {
	case resp := <-respC:
		return resp.err
	case <-time.After(blb.lbResponseAwaitTimeout):
	}

	return fmt.Errorf("timeout while waiting for load balancer response, upstream may be registered: %s", providerID)
}

func (blb *baseLoadbalancer) handleRegister(req *registerRequest) {
	resp := &registerResponse{}

	currProvidersLen := uint(len(blb.providers))
	if currProvidersLen >= blb.limit {
		resp.err = fmt.Errorf("maximum amount of providers already registered: %d", blb.limit)
	} else {
		blb.providers = append(blb.providers, provider.NewProvider(req.id, req.providerType))
		log.Printf("Provider registered (id: %s)", req.id)
	}

	select {
	case req.respChan <- resp:
	default:
		log.Printf("sending response to register request would block, dropping (id: %s)", req.id)
	}
}

func (blb *baseLoadbalancer) Exclude(providerID string) error {
	respC := make(chan struct{}, 1)
	select {
	case blb.excludeChan <- &excludeRequest{respChan: respC, id: providerID}:
	case <-time.After(blb.getRequestSendTimeout):
		return fmt.Errorf("Timeout while sending exclude request to load balancer: %s", providerID)
	}

	select {
	case <-respC:
	case <-time.After(blb.lbResponseAwaitTimeout):
		return fmt.Errorf("timeout while waiting for load balancer response, upstream may not be deleted: %s", providerID)
	}

	return nil
}

func (blb *baseLoadbalancer) handleExclude(req *excludeRequest) {
	// note that this is a naive implementation with linear complexity in respect to amount of providers
	// in real world case I would use a doubly linked list holding each provider
	// and a mapping between id and a pointer to said list node
	// this way provider could be deleted in constant time
	// theres also no name validation, so in theory this loop could delete multiple providers
	for i, prov := range blb.providers {
		if prov.Get() != req.id {
			continue
		}
		blb.providers = append(blb.providers[:i], blb.providers[i+1:]...)

		log.Printf("Provider removed (id: %s)", req.id)
		break
	}

	select {
	case req.respChan <- struct{}{}:
	default:
		log.Printf("responding to exclude request would block, dropping (id: %s)", req.id)
	}
}

func (blb *baseLoadbalancer) handleHealthCheck() {
	log.Printf("Running scheduled healthcheck")
	// yet again this operation would have better complexity characteristics on linked list
	// see comment in handleExclude
	providers := make([]provider.Provider, 0, len(blb.providers))
	faultyProviders := make([]provider.Provider, 0, len(blb.providers)/2)
	for _, prov := range blb.providers {
		if prov.Check() != nil {
			faultyProviders = append(faultyProviders, prov)
			continue
		}
		providers = append(providers, prov)
	}
	blb.providers = providers

	if len(faultyProviders) != 0 {
		log.Printf("Evicted faulty providers (providers: %+v)", faultyProviders)
	}

	for prov, cnt := range blb.faultyProviders {
		if prov.Check() != nil {
			blb.faultyProviders[prov] = 0
			continue
		}
		cnt++

		if cnt >= blb.healthyRecheckThreshold {
			delete(blb.faultyProviders, prov)
			blb.providers = append(blb.providers, prov)
			log.Printf("provider included back to healthy pool (id: %s)", prov.Get())
			continue
		}
		blb.faultyProviders[prov] = cnt
	}

	for _, prov := range faultyProviders {
		blb.faultyProviders[prov] = 0
	}
	log.Printf("faulty providers after healthcheck run: %+v", blb.faultyProviders)
}
