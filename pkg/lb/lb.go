package lb

import (
	"context"
	"errors"
	"time"

	"gitlab.com/tjonak/load-balancer/pkg/provider"
)

// Type represents load balancer type, which codes upstream selection behavior
type Type string

const (
	// TypeRandom represents load balancer type with random upstream selection
	TypeRandom = Type("Random")
	// TypeRoundRobin represents load balancer type with round robin upstream selection
	TypeRoundRobin = Type("RoundRobin")
)

// ErrRateLimitExceeded is returned when lb receives a request but has no registered providers
var ErrRateLimitExceeded = errors.New("rate limit exceeded")

// LoadBalancer represents any loadbalancer
type LoadBalancer interface {
	Run(context.Context)
	Get() (string, error)
	Register(string) error
	RegisterTyped(string, provider.Type) error
	Exclude(string) error
}

// NewLoadBalancer creates load balancer instance of given type
func NewLoadBalancer(
	t Type,
	limit uint,
	healthCheckInterval time.Duration,
	maxConcurrentRequestsPerProvider uint32,
) LoadBalancer {
	var us upstreamSelector
	switch t {
	case TypeRoundRobin:
		us = newRoundRobinUpstreamSelector()
	default:
		us = newRandomUpstreamSelector()
	}

	return newLoadBalancer(limit, us, healthCheckInterval, maxConcurrentRequestsPerProvider)
}
