package main

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"gitlab.com/tjonak/load-balancer/pkg/lb"
	"gitlab.com/tjonak/load-balancer/pkg/provider"
)

func main() {
	for {
		fmt.Println(">>>>>>>> choose a step, 3-8 available, invalid choice exits.")
		var choice int
		_, err := fmt.Scan(&choice)
		if err != nil {
			return
		}
		switch choice {
		case 3:
			step3()
		case 4:
			step4()
		case 5:
			step5()
		case 6:
			step6()
		case 7:
			step7()
		case 8:
			step8()
		default:
			return
		}
	}
}

func step8() {
	const providersLimit = 10
	const healthCheckInterval = time.Second * 5

	lb := lb.NewLoadBalancer(lb.TypeRoundRobin, providersLimit, healthCheckInterval, 3)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go lb.Run(ctx)

	for i := 0; i < 3; i++ {
		err := lb.RegisterTyped(fmt.Sprintf("p%d", i), provider.TypeSlow)
		if err != nil {
			log.Fatal(err)
		}
	}

	wg := &sync.WaitGroup{}
	defer wg.Wait()

	for i := 0; i < 12; i++ {
		wg.Add(1)
		go func(id int) {
			res, err := lb.Get()
			if err != nil {
				log.Printf("req: %d, err: %v", id, err)
			} else {
				log.Println(res)
			}
			wg.Done()
		}(i)
	}
}

func step7() {
	const providersLimit = 10
	const healthCheckInterval = time.Second * 5

	lb := lb.NewLoadBalancer(lb.TypeRoundRobin, providersLimit, healthCheckInterval, 10)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go lb.Run(ctx)

	for i := 0; i < 5; i++ {
		err := lb.Register(fmt.Sprintf("p%d", i))
		if err != nil {
			log.Fatal(err)
		}
	}

	err := lb.RegisterTyped("fp", provider.TypeFaulty)
	if err != nil {
		log.Fatal(err)
	}

	err = lb.RegisterTyped("fptr", provider.TypeFaultyThatRecovers)
	if err != nil {
		log.Fatal(err)
	}

	<-time.After(healthCheckInterval)

	for i := 0; i < 6; i++ {
		res, err := lb.Get()
		if err != nil {
			log.Fatal(err)
		}
		log.Println(res)
	}

	<-time.After(healthCheckInterval * 3)

	for i := 0; i < 6; i++ {
		res, err := lb.Get()
		if err != nil {
			log.Fatal(err)
		}
		log.Println(res)
	}
}

func step6() {
	const providersLimit = 10
	const healthCheckInterval = time.Second * 5
	lb := lb.NewLoadBalancer(lb.TypeRoundRobin, providersLimit, healthCheckInterval, 10)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go lb.Run(ctx)

	for i := 0; i < 5; i++ {
		err := lb.Register(fmt.Sprintf("p%d", i))
		if err != nil {
			log.Fatal(err)
		}
	}

	err := lb.RegisterTyped("fp", provider.TypeFaulty)
	if err != nil {
		log.Fatal(err)
	}

	<-time.After(healthCheckInterval)

	for i := 0; i < 6; i++ {
		res, err := lb.Get()
		if err != nil {
			log.Fatal(err)
		}
		log.Println(res)
	}
}

func step5() {
	const providersLimit = 10
	lb := lb.NewLoadBalancer(lb.TypeRoundRobin, providersLimit, time.Duration(0), 10)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go lb.Run(ctx)

	_, err := lb.Get()
	if err == nil {
		log.Fatal("No error on get without registered providers")
	}

	for i := 0; i < providersLimit; i++ {
		err := lb.Register(fmt.Sprintf("p%d", i))
		if err != nil {
			log.Fatal(err)
		}
	}

	doRequests := func(n int) {
		for i := 0; i < n; i++ {
			res, err := lb.Get()
			if err != nil {
				log.Fatal(err)
			}
			log.Println(res)
		}
	}

	doRequests(6)

	lb.Exclude("p4")
	lb.Exclude("pp")

	doRequests(5)

	err = lb.Register("p10")
	if err != nil {
		log.Fatal(err)
	}

	doRequests(10)
}

func step4() {
	const providersLimit = 10
	lb := lb.NewLoadBalancer(lb.TypeRoundRobin, providersLimit, time.Duration(0), 10)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go lb.Run(ctx)

	_, err := lb.Get()
	if err == nil {
		log.Fatal("No error on get without registered providers")
	}

	for i := 0; i < providersLimit; i++ {
		err := lb.Register(fmt.Sprintf("p%d", i))
		if err != nil {
			log.Fatal(err)
		}
	}

	for i := 0; i < providersLimit*2; i++ {
		res, err := lb.Get()
		if err != nil {
			log.Fatal(err)
		}
		log.Println(res)
	}
}

func step3() {
	const providersLimit = 10
	lb := lb.NewLoadBalancer(lb.TypeRandom, providersLimit, time.Duration(0), 10)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go lb.Run(ctx)

	_, err := lb.Get()
	if err == nil {
		log.Fatal("No error on get without registered providers")
	}

	for i := 0; i < providersLimit; i++ {
		err := lb.Register(fmt.Sprintf("p%d", i))
		if err != nil {
			log.Fatal(err)
		}
	}

	for i := 0; i < providersLimit*2; i++ {
		res, err := lb.Get()
		if err != nil {
			log.Fatal(err)
		}
		log.Println(res)
	}
}
